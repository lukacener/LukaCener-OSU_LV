from PIL import Image
import numpy as np
from tensorflow import keras
import matplotlib.pyplot as plt
from keras.models import load_model

model=load_model('FCN/')

image_path = 'test_on_8.png'

image_number = image_path[8]

image = Image.open(image_path).convert('L')
image = image.resize((28, 28))  
image_array = np.array(image)

image_array = image_array.astype("float32") / 255
image_array = np.expand_dims(image_array, axis=0)
image_array = np.expand_dims(image_array, axis=-1)

predicted_label = np.argmax(model.predict(image_array))

print(f"Real label: {image_number}, Predicted label: {predicted_label}")

# Real label: 0, Predicted label: 0
# Real label: 1, Predicted label: 7
# Real label: 2, Predicted label: 2
# Real label: 3, Predicted label: 3
# Real label: 4, Predicted label: 4
# Real label: 5, Predicted label: 3
# Real label: 6, Predicted label: 2
# Real label: 7, Predicted label: 7
# Real label: 8, Predicted label: 8
# Real label: 9, Predicted label: 6