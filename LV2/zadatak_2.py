import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('data.csv', delimiter = ",", dtype = "str")
data = data[1::]
data = np.array(data, np.float64)

#a)
print(f"Amount of people measurred: {len(data)}\n")

#b)
height = data[:, 1] 
weight = data[:, 2]
plt.scatter(height, weight)
plt.show()

#c)
height = data[0::50, 1]
weight = data[0::50, 2]
plt.scatter(height, weight)
plt.show()

#d)
height = data[:, 1] 
mean = height.mean()
max = height.max()
min = height.min()
print(f"Max height: {max}")
print(f"Min height: {min}")
print(f"Average height: {mean}\n")

#e)
men = data[data[:,0] == 1]
women = data[data[:,0] == 0]
print(f"Men:\n Max height: {men[:, 1].max()}\n Min height: {men[:, 1].min()}\n Average height: {men[:, 1].mean()}\n")
print(f"Women:\n Max height: {women[:, 1].max()}\n Min height: {women[:, 1].min()}\n Average height:: {women[:, 1].mean()}")