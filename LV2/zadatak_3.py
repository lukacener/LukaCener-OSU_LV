import numpy as np
import matplotlib.pyplot as plt

img = plt.imread("road.jpg")
image = img.copy()

#a)
plt.imshow(image, alpha=0.5)
plt.show ()

#b)
width = len(image[0])
quarter_width = int(width/4)
plt.imshow(image[:, 1*quarter_width: 2*quarter_width, :])
plt.show()

#c)
rt_image = np.rot90(image,3)
plt.imshow(rt_image)
plt.show()

#d)
fp_image = np.flip(image, 1)
plt.imshow(fp_image)
plt.show()