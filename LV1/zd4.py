word_count = {}

song_file = open("song.txt")
for line in song_file:
    line = line.rstrip()
    words = line.split(" ")
    for word in words:
        if word not in word_count:
            word_count[word] = 1
            continue
        word_count[word] += 1

for word, count in word_count.items():
    if count == 1:
        print(f"{word}")

song_file.close()