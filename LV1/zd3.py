numbers = []

while True:
    try:
        user_input = input("Unesite broj ili „Done“: ")

        if user_input == "Done":
            break

        number = float(user_input)
        numbers.append(number)
        
    except:
        print("Pogrešan unos. Unesite broj ili „Done“.")

if numbers:
    if len(numbers) == 1:
        print(f"Korisnik je unio {len(numbers)} broj.")
    else:
        print(f"Korisnik je unio {len(numbers)} broja.")

    print(f"Srednja vrijednost brojeva je {sum(numbers) / len(numbers)}.")
    print(f"Minimalna vrijednost brojeva je {min(numbers)}.")
    print(f"Maksimalna vrijednost brojeva je {max(numbers)}.")

    numbers.sort()
    print("Sortirana lista brojeva:", numbers)
    
else:
    print("Nema brojeva.")
