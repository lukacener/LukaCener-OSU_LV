def average_word_count(sms_list):
    total_words = 0
    for sms in sms_list:
        total_words += len(sms.split(" "))
    return total_words / len(sms_list)

ham = []
spam = []
spam_with_exclamation_mark = 0

sms_file = open("SMSSpamCollection.txt")
for line in sms_file:
    line = line.rstrip()
    parts = line.split("\t")
    if (parts[0] == "ham"):
        ham.append(parts[1])
    elif (parts[0] == "spam"):
        spam.append(parts[1])
        sms = parts[1]
        if (sms[-1] == '!'):
            spam_with_exclamation_mark += 1

print(f"Average word count in spam {average_word_count(spam)}")
print(f"Average word count in ham {average_word_count(ham)}")
print(f"Number of spam ending with !: {spam_with_exclamation_mark}")

sms_file.close()