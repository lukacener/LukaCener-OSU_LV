try:
    num = float(input("Unesi broj između 0.0 i 1.0: "))

    if 0.0 <= num <= 1.0:
        if num >= 0.9:
            print("Ocjena: A")
        elif num >= 0.8:
            print("Ocjena: B")
        elif num >= 0.7:
            print("Ocjena: C")
        elif num >= 0.6:
            print("Ocjena: D")
        else:
            print("Ocjena: F")
    else:
        print("Uneseni broj nije između 0.0 i 1.0.")
        
except:
    print("Pogrešan unos. Unesi broj između 0.0 i 1.0.")