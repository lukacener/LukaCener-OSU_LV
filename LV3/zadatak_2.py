import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_csv('data_C02_emission.csv')

#a)
data['CO2 Emissions (g/km)'].plot(kind='hist', bins=20)
plt.show()

#b)
colors = {'Z': 'green', 'X': 'red', 'E': 'blue', 'D': 'black'}
data.plot.scatter(x="Fuel Consumption City (L/100km)", y="CO2 Emissions (g/km)", c=data["Fuel Type"].map(colors), s=25)
plt.show()

#c)
data.boxplot(column=["Fuel Consumption Hwy (L/100km)"], by="Fuel Type")
plt.show()

#d)
fuel_grouped_num = data.groupby('Fuel Type').size()
fuel_grouped_num.plot(kind ='bar', xlabel='Tip goriva', ylabel='Broj vozila', title='Kolicina vozila po tipu goriva')
plt.show()

#e)
cylinder_grouped = data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()
cylinder_grouped.plot(kind='bar', x=cylinder_grouped.index, y=cylinder_grouped.values, xlabel='Broj cilindara', ylabel='CO2 emisija (g/km)', title='CO2 emisija po broju cilindara')
plt.show()