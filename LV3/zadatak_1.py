import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#a)
data = pd.read_csv("data_C02_emission.csv")
print(len(data))
print(data.info())

print(f"\nIzostale vrijednosti u redovima:\n{data.isnull().sum()}\n")
print(f"Duplicirane vrijednosti: {data.duplicated().sum()}")
data['Vehicle Class'] = data['Vehicle Class'].astype('category')

#b)
print(f"Najveca gradska potrosnja:\n{data[['Make', 'Model', 'Fuel Consumption City (L/100km)']].nlargest(3, 'Fuel Consumption City (L/100km)')}")
print(f"Najmanja gradska potrosnja:\n{data[['Make', 'Model', 'Fuel Consumption City (L/100km)']].nsmallest(3, 'Fuel Consumption City (L/100km)')}")

#c) 
selected_data = data[(data['Engine Size (L)'] >= 2.5) & (data['Engine Size (L)'] <= 3.5)]
print(f"Postoji {len(selected_data['Make'])} vozila koje imaju motor izmedu 2.5 i 3.5 L")
print(f"Prosjecna emisija CO2 plinova: {selected_data['CO2 Emissions (g/km)'].mean()} g/km")

#d)
selected_data = data[(data['Make'] == 'Audi')]
print(f"Na Audi se odnosi {len(selected_data['Make'])} mjerenja.")

selected_data = selected_data[(selected_data['Cylinders'] == 4)]
print(f"Prosjecna emisija CO2 plinova Audi automobila s 4 cilindra: {selected_data['CO2 Emissions (g/km)'].mean()} g/km")

#e)
cylinder_counts = data['Cylinders'].value_counts().sort_index()
print(cylinder_counts)

cylinder_emissions = data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()
print("Prosjecna emisija CO2 plinova s obzirom na broj cilindara: ")
print(cylinder_emissions)

#f)
diesels = data[(data['Fuel Type'] == 'D')]
petrols = data[(data['Fuel Type'] == 'Z')]

print(f"Dizel:\nProsjecno: {diesels['Fuel Consumption City (L/100km)'].mean()}\nMedijalno: {diesels['Fuel Consumption City (L/100km)'].median()}")
print(f"Benzin:\nProsjecno: {petrols['Fuel Consumption City (L/100km)'].mean()}\nMedijalno: {petrols['Fuel Consumption City (L/100km)'].median()}")

#g)
four_cylinder_diesels = diesels[(diesels['Cylinders'] == 4)]
print(f"Automobil s 4 cilindra koji koristi dizelski motor koji ima najvecu gradsku potrosnju goriva:\n{four_cylinder_diesels.nlargest(1, 'Fuel Consumption City (L/100km)')}")

#h)
manuals = data[(data['Transmission'].str[0] == 'M')]
print(f"Postoji {len(manuals['Make'])} vozila s rucnim mjenjacem.")

#i)
print(data.corr(numeric_only=True))